from .actor import SubprocessManager
from .views import SubprocessView, LauncherToolBar

from . import _version
__version__ = _version.get_versions()['version']
